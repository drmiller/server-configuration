# Server Configuration
### Scripts to configure an Ubuntu server

1.  Get this repository
```
mkdir ~/git
cd ~/git
git clone https://drmiller@bitbucket.org/drmiller/server-configuration.git
cd server-configuration
```

2.  Update Ubuntu
```
sudo ./update_ubuntu.sh
```

3.  Setup SSH
```
ssh-keygen
more ~/.ssh/id_rsa.pub
```
Add to Bitbucket SSH Keys

4.  Get Anaconda
```
./install_anaconda.sh
```