# Create new environment and make default
conda create --name python3
conda install -y -n python3 --file python_packages.txt
echo 'source activate python3' >> ~/.bashrc
source .bashrc
